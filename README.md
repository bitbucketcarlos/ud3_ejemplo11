# Ud3_Ejemplo11
_Ejemplo 11 de la Unidad 3._ Paso de objetos entre Actividades.

Vamos a enviar objetos desde una Actividad a otra usando _Intents_ explícitos con datos extras. Para ello crearemos una clase Persona, 
rellenaremos sus datos y los enviaremos para que los muestre otra Atividad.

## _Persona.kt_
Creamos la clase Persona con solo dos atributos. Ésta tiene que implementar la interfaz _Parcelable_ de tal forma que pueda ser 
convertida y enviada a través del _Intent_. Para ello es necesario implementar el atributo _CREATOR_ e implmenetar las funciones _describeContents_ y _writeToParcel_.
```java
data class Persona(val nombre: String?, val apellido: String?) : Parcelable {

    companion object CREATOR: Parcelable.Creator<Persona> {
        override fun createFromParcel(`in`: Parcel): Persona {
            return Persona(`in`)
        }

        override fun newArray(size: Int): Array<Persona?> {
            return arrayOfNulls(size)
        }
    }

    constructor(`in`: Parcel) : this(`in`.readString(), `in`.readString())

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(out: Parcel, flag: Int) {
        out.writeString(nombre)
        out.writeString(apellido)

    }
}

```
## _activity_main.xml_ y _actividad2.xml_
Creamos los layout de las dos Actividades:

_activity_main.xml_:
```html
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <TextView
        android:id="@+id/textNombre"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/nombre"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintHorizontal_bias="0.236"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.358" />

    <EditText
        android:id="@+id/editTextNombre"
        android:layout_width="220dp"
        android:layout_height="wrap_content"
        android:hint="@string/nombre"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.4"
        app:layout_constraintStart_toEndOf="@+id/textNombre"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.354" />

    <TextView
        android:id="@+id/textApellido"
        android:layout_width="56dp"
        android:layout_height="24dp"
        android:text="@string/apellido"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="@+id/textNombre"
        app:layout_constraintStart_toStartOf="@+id/textNombre"
        app:layout_constraintTop_toBottomOf="@+id/textNombre"
        app:layout_constraintVertical_bias="0.22000003" />

    <EditText
        android:id="@+id/editTextApellido"
        android:layout_width="220dp"
        android:layout_height="wrap_content"
        android:hint="@string/apellido"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="@+id/editTextNombre"
        app:layout_constraintStart_toStartOf="@+id/editTextNombre"
        app:layout_constraintTop_toBottomOf="@+id/editTextNombre"
        app:layout_constraintVertical_bias="0.17000002" />

    <Button
        android:id="@+id/boton"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/enviar"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/editTextApellido" />
</androidx.constraintlayout.widget.ConstraintLayout>
```
_actividad2.xml_:
```html
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".Actividad2">

    <TextView
        android:id="@+id/nombreAct2"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintVertical_bias="0.36"
        tools:text="@string/nombre" />

    <TextView
        android:id="@+id/apellidoAct2"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/nombreAct2"
        app:layout_constraintVertical_bias="0.26"
        tools:text="@string/apellido" />

</androidx.constraintlayout.widget.ConstraintLayout>
```
## _MainActivity.kt_
En el fichero _MainActivity.kt_ creamos el objeto de la clase Bundle y le asignamos el par (clave, valor) siendo la 
clave _Persona_ y el valor el objeto _persona_, añadimos los datos extra que el _Intent_ pasará y lo enviamos.
```java
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        binding.boton.setOnClickListener {
            // Creamos el objeto de la clase Persona.
            val persona = Persona(binding.editTextNombre.text.toString(),binding.editTextApellido.text.toString())

            // Creamos el objeto de la clase Bundle y le asignamos el par (clave, valor).
            val bundle = Bundle()

            bundle.putParcelable("Persona", persona)

            // Creamos el Intent, le añadimos el objeto bundle y lo enviamos.
            val intent = Intent(this, Actividad2::class.java).apply {
                putExtra(Intent.EXTRA_TEXT, bundle)
            }

            startActivity(intent)
        }
    }
}

```
## _Actividad2.kt_
Por último, Actividad2 recogerá el objeto Bundle y obtendrá el valor a partir de la clase _Persona_ y lo mostrará en los _TextViews_.
```java
class Actividad2 : AppCompatActivity() {

    private lateinit var binding: Actividad2Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = Actividad2Binding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        if(intent.hasExtra(Intent.EXTRA_TEXT)) {
            // Recogemos el objeto bundle y obtenemos el objeto persona usando la clave "Persona".
            val bundle = intent.getBundleExtra(Intent.EXTRA_TEXT)

            val persona:Persona? = bundle?.getParcelable("Persona")

            // Buscamos y rellenamos los TextView para mostrar los datos.
            binding.nombreAct2.text = persona?.nombre
            binding.apellidoAct2.text = persona?.apellido

        }
    }
}
```
