package com.example.ud3_ejemplo11

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.ud3_ejemplo11.databinding.Actividad2Binding

class Actividad2 : AppCompatActivity() {

    private lateinit var binding: Actividad2Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = Actividad2Binding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        if(intent.hasExtra(Intent.EXTRA_TEXT)) {
            // Recogemos el objeto bundle y obtenemos el objeto persona usando la clave "Persona".
            val bundle = intent.getBundleExtra(Intent.EXTRA_TEXT)

            val persona:Persona? = bundle?.getParcelable("Persona")

            // Buscamos y rellenamos los TextView para mostrar los datos.
            binding.nombreAct2.text = persona?.nombre
            binding.apellidoAct2.text = persona?.apellido

        }
    }
}