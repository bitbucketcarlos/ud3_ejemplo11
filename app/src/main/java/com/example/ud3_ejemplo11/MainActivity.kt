package com.example.ud3_ejemplo11

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.ud3_ejemplo11.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        binding.boton.setOnClickListener {
            // Creamos el objeto de la clase Persona.
            val persona = Persona(binding.editTextNombre.text.toString(),binding.editTextApellido.text.toString())

            // Creamos el objeto de la clase Bundle y le asignamos el par (clave, valor).
            val bundle = Bundle()

            bundle.putParcelable("Persona", persona)

            // Creamos el Intent, le añadimos el objeto bundle y lo enviamos.
            val intent = Intent(this, Actividad2::class.java).apply {
                putExtra(Intent.EXTRA_TEXT, bundle)
            }

            startActivity(intent)
        }
    }
}