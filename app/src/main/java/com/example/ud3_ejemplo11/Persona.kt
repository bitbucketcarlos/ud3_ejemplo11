package com.example.ud3_ejemplo11

import android.os.Parcel
import android.os.Parcelable

/* Implementamos la interfaz Parcelable para poder pasar el objeto creado de una actividad a otra.
   Para ello es necesario implementar el atributo CREATOR e implementar las funciones describeContents
   y writeToParcel. */
data class Persona(val nombre: String?, val apellido: String?) : Parcelable {

    companion object CREATOR: Parcelable.Creator<Persona> {
        override fun createFromParcel(`in`: Parcel): Persona {
            return Persona(`in`)
        }

        override fun newArray(size: Int): Array<Persona?> {
            return arrayOfNulls(size)
        }
    }

    constructor(`in`: Parcel) : this(`in`.readString(), `in`.readString())

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(out: Parcel, flag: Int) {
        out.writeString(nombre)
        out.writeString(apellido)

    }
}